package practical.strings;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Task3 {
	public static void main(String[] args) throws IOException {
		File f = new File("data/holmes.txt");
		FileReader in = new FileReader(f);
		StringBuilder raw = new StringBuilder(); 
		for (int c = 0; c != -1;) {
			c = in.read();
			raw.append((char)c);
		}
		in.close();
		
		StringBuilder encoded = new StringBuilder();
		for(int i = 0; i < raw.length(); ++i) {
			char c = raw.charAt(i);
			if (Character.isLetter(c)) {
				encoded.append(cypher(Character.toUpperCase(c)));
			}
		}

		StringBuilder spaced = new StringBuilder();
		for(int i = 0; i < encoded.length(); ++i) {
			char c = encoded.charAt(i);
			spaced.append(c);
			if ((i % 5) == 4) {
				spaced.append(' ');
			}
		}
		
		int leftover = encoded.length() % 5;
		int pad = 5 - leftover;
		for (int i = 0; i < pad; ++i) {
			spaced.append('X');
			
		}
		System.out.println(spaced);
	}
	
	private static char cypher(char c) {
		int pos = c - 'A';
		int bumped = pos + 3;
		int wrapped = bumped % 26;
		return (char)('A' + wrapped);
	}
}
