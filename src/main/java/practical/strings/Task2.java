package practical.strings;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Task2 {
	public static void main(String[] args) throws IOException {
		File f = new File("data/holmes.txt");
		FileReader in = new FileReader(f);
		StringBuilder raw = new StringBuilder(); 
		
		for (int c = 0; c != -1;) {
			c = in.read();
			raw.append((char)c);
		}
		in.close();
		
		StringBuilder stripped = new StringBuilder();
		for(int i = 0; i < raw.length(); ++i) {
			char c = raw.charAt(i);
			if (Character.isLetter(c)) { // Note, the cypher can't handle digits :(
				stripped.append(cypher(Character.toUpperCase(c)));
			}
		}
		System.out.println(stripped);
	}
	
	private static char cypher(char c) {
		int pos = c - 'A';
		int bumped = pos + 3;
		int wrapped = bumped % 26;
		return (char)('A' + wrapped);
	}
}
