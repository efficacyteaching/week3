package practical.strings;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Task1 {
	public static void main(String[] args) throws IOException {
		File f = new File("data/holmes.txt");
		FileReader in = new FileReader(f);
		StringBuilder raw = new StringBuilder(); 
		
		for (int c = 0; c != -1;) {
			c = in.read();
			raw.append((char)c);
		}
		in.close();
//		System.out.println(raw);
		
		StringBuilder stripped = new StringBuilder();
		for(int i = 0; i < raw.length(); ++i) {
			char c = raw.charAt(i);
			if (Character.isLetterOrDigit(c)) {
				stripped.append(c);
			}
		}
		System.out.println(stripped);
	}
}
